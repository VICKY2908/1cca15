package programmingbatch;

public class Arrayprogram {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};

        for (int a : arr)    //enhanced for loop
        {
            System.out.println(a);
        }
    }
}