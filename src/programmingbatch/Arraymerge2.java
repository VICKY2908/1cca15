package programmingbatch;

import java.util.Arrays;

public class Arraymerge2 {
    public static void main(String[] args) {
        int arr1[]={1,2,3,4};
        int arr2[]={5,6,7,8};
        int n1=arr1.length;
        int n2= arr2.length;
        int[] arr3=new int[n1+n2];
        int indx=0;
        for (int a:arr1){

            arr3[indx]=a;
            indx+=2;
        }
        indx=1;
        for (int a:arr2){
            arr3[indx]=a;
            indx+=2;
        }
        System.out.println(Arrays.toString(arr3));
    }
}
